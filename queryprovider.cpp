#include <cstring>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <windows.h>
#include <winsock2.h>
#include <objbase.h>
#include <SvcGuid.h>
#include <Ws2spi.h>

#include "helpers.h"

using namespace std;

#define CHECK_WSTRING(x) ((x != 0) ? x : L"<empty>")
#define CHECK_STRING(x) ((x != 0) ? x : "<empty>")
#define CHECK_RESULT(r, v) if(r == v) cout << "error was " << #v << endl;

void gethostbynameTest(const std::string& queryString) {
    std::cout << "<" << queryString.c_str() << ">" << std::endl;
    struct hostent* remoteHost = gethostbyname(queryString.c_str());
    if (remoteHost && remoteHost->h_addrtype == AF_INET) {
        std::cout << "remoteHost: " << std::hex << (int)remoteHost << " "
                    << (BYTE*)remoteHost->h_name << " " << std::hex << (int)remoteHost->h_name << std::endl;
        std::cout << "aliases: " << std::hex << (int)remoteHost->h_aliases << " alias[0]: " << (int)remoteHost->h_aliases[0] << std::endl;
        int j = 0;
        while(remoteHost->h_aliases[j] != 0) {
            std::cout << "\talias[" << j << "]: " << remoteHost->h_aliases[j++] << std::endl;
        }
        std::cout << "address length: " << (int)remoteHost->h_length << std::endl;
        std::cout << "address list: " << std::hex << (int)remoteHost->h_addr_list << std::endl;
        int i = 0;
        while (remoteHost->h_addr_list[i] != 0) {
            struct in_addr addr;
            addr.s_addr = *(u_long *) remoteHost->h_addr_list[i++];
            cout << "\tIPv4 Address #" << i << ": " << inet_ntoa(addr) << " " << std::hex << (int)*(int*)remoteHost->h_addr_list[(i - 1)] << endl;
        }
    } else if(remoteHost == 0) {
        cout << "gethostbyname failed!" << endl;
    }
}

void lookupServiceTest(const std::string& queryString, GUID *gptr) {
    WSAQUERYSETW qs, *qs_return;
    ZeroMemory(&qs, sizeof(WSAQUERYSETW));

    qs.dwSize = sizeof(WSAQUERYSETW);
    int qss = queryString.size();
    qs.lpszServiceInstanceName = new WCHAR[qss + 1];
    ZeroMemory(qs.lpszServiceInstanceName, sizeof(WCHAR)*(qss + 1));
    std::wstring s = widenString(queryString);
    wcsncpy(qs.lpszServiceInstanceName, s.c_str(), qss + 1);

    GUID g = SVCID_INET_HOSTADDRBYNAME;
    qs.lpServiceClassId = &g;
    qs.dwNameSpace = NS_DNS;
    if(gptr != NULL) *qs.lpNSProviderId = *gptr;

    wcout << L"Before:" << endl;
    wcout << dumpQuerySet(&qs) << endl;

    HANDLE h;
    int result = 0;
    DWORD dwFlags = LUP_DUAL_ADDR|LUP_ADDRCONFIG|LUP_RETURN_ADDR|LUP_RETURN_NAME;
    if(WSALookupServiceBeginW(&qs, dwFlags, &h) == SOCKET_ERROR) {
        result = WSAGetLastError();
        CHECK_RESULT(result, WSA_NOT_ENOUGH_MEMORY)
        CHECK_RESULT(result, WSAEFAULT)
        CHECK_RESULT(result, WSAEINVAL)
        CHECK_RESULT(result, WSANO_DATA)
        CHECK_RESULT(result, WSAEOPNOTSUPP)
        CHECK_RESULT(result, WSANOTINITIALISED)
        CHECK_RESULT(result, WSASERVICE_NOT_FOUND)
        wcout << L"WSALookupServiceBeginW failure result: " << result << endl;
    }
    wcout << endl;

    DWORD size = 1024;
    qs_return = (WSAQUERYSETW*)new BYTE[size];

    int r = NO_ERROR;
    while(r == NO_ERROR) {
        r = WSALookupServiceNextW(h, dwFlags, &size, qs_return);
        if(r != NO_ERROR) {
            result = WSAGetLastError();
            CHECK_RESULT(result, WSA_NOT_ENOUGH_MEMORY)
            CHECK_RESULT(result, WSAEFAULT)
            CHECK_RESULT(result, WSAEINVAL)
            CHECK_RESULT(result, WSANO_DATA)
            CHECK_RESULT(result, WSAEOPNOTSUPP)
            CHECK_RESULT(result, WSANOTINITIALISED)
            CHECK_RESULT(result, WSASERVICE_NOT_FOUND)
            CHECK_RESULT(result, WSA_E_CANCELLED)
            CHECK_RESULT(result, WSA_E_NO_MORE)
            CHECK_RESULT(result, WSAHOST_NOT_FOUND)

            if(result == WSAEFAULT) {
                wcout << L"failed due to: WSAEFAULT; size:" << size << endl;
                delete[] (BYTE*)qs_return;
                qs_return = (WSAQUERYSETW*)new BYTE[size];
                continue;
            }
            wcout << L"WSALookupServiceNextW failed result: " << result << " " << WSA_E_CANCELLED << endl;
            break;
        }

        wcout << L"After:" << endl;
        wcout << dumpQuerySet(qs_return);
    }
}

void displayHelp(const std::string& argv0) {
    cout << "Test application for querying Windows namespace providers" << endl
         << "Syntax:" << endl
         << "\t" << argv0 << " [-h|--help]" << endl
         << "\t" << argv0 << " [-gethostbyname]" << " [-guid {...}]" << " name" << endl
         << endl
         << "Parameters:" << endl
         << "\t" << "-gethostbyname" << "\t" << "uses gethostbyname function to retrieve ip address" << endl
         << "\t" << "-guid {...}" << "   \t" << "uses the provider with a certain guid to retrieve the" << endl
         << "\t\t\t"                         << "ip address" << endl
         << "\t" << "-h | --help" << "   \t" << "display this help and exit" << endl;
}

int main(int argc, char** argv) {
    std::vector<std::string> args(argv + 1, argv + argc);
    std::stringstream ss;
    std::string queryString; //= std::string("snorri.local\0\0");
    bool use_gethostbyname = false;
    bool use_displayHelp = false;
    GUID guid, *gptr = NULL;
    // only for now:
    std::wcout << L"query NameSpaceProvider" << std::endl;
    for(std::vector<string>::iterator it = args.begin(); it != args.end(); it++) {
        if(*it == "-guid") {
            it++;
            stringToGUID(*it, &guid);
            gptr = &guid;
            continue;
        }

        if(*it == "-gethostbyname") {
            use_gethostbyname = true;
            continue;
        }

        if(*it == "--help" || *it == "-h") {
            use_displayHelp = true;
        }

        queryString = std::string(*it);
    }

    if(args.size() == 0 || use_displayHelp) {
        displayHelp(argv[0]);
        return 1;
    }


    WSADATA wd;
    if (WSAStartup(MAKEWORD(2, 2), &wd) != 0) {
        std::wcout << L"WSAStartup failed" << std::endl;
        return 1;
    }

    if(use_gethostbyname) {
        gethostbynameTest(queryString);
        return 0;
    }

    lookupServiceTest(queryString, gptr);
    WSACleanup();
    return 0;
}
