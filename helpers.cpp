#include "helpers.h"

#include <iostream>
#include <sstream>
#include <locale>
#include <vector>
#include <map>
#include <winsock2.h>
#include <windows.h>
#include <ws2spi.h>
#include <Ws2ipdef.h>
#include <Sporder.h>

#define CHECK_RESULT(r, v) if(r == v) std::cout << "error was " << #v << std::endl;


bool stringToGUID(const std::string& guid, GUID* output) {
    std::stringstream s(guid.substr(1));
    __int64 l = 0;
    __int64 ll = 0;
    char c;
    ZeroMemory(&output->Data1, 4);
    ZeroMemory(&output->Data2, 4);
    ZeroMemory(&output->Data3, 4);
    ZeroMemory(output->Data4, 8);
    try {
        s >> std::hex >> output->Data1 >> c
          >> std::hex >> output->Data2 >> c
          >> std::hex >> output->Data3 >> c
          >> std::hex >> l >> c >> std::hex >> ll;
          ll += l << (6*8);
        for(int i = 7; i >= 0; i--) {
            output->Data4[i] = ll % 256; ll >>= 8;
        };
    } catch(...) {
        return false;
    }
    return true;
}

std::wstring guidToWString(GUID* guid) {
    wchar_t output[41];

    if(guid == 0) return std::wstring();

    _snwprintf(output, 40, L"{%08X-%04hX-%04hX-%02X%02X-%02X%02X%02X%02X%02X%02X}", guid->Data1, guid->Data2, guid->Data3, guid->Data4[0], guid->Data4[1], guid->Data4[2], guid->Data4[3], guid->Data4[4], guid->Data4[5], guid->Data4[6], guid->Data4[7]);
    return std::wstring(output);
}

std::string guidToString(GUID* guid) {
    char output[81];

    if(guid == 0) return std::string();

    _snprintf(output, 40, "{%08X-%04hX-%04hX-%02X%02X-%02X%02X%02X%02X%02X%02X}", guid->Data1, guid->Data2, guid->Data3, guid->Data4[0], guid->Data4[1], guid->Data4[2], guid->Data4[3], guid->Data4[4], guid->Data4[5], guid->Data4[6], guid->Data4[7]);
    return std::string(output);
}

std::wstring widenString(std::string n) {
    std::wstring ws;
    std::locale loc;
    wchar_t* w = new wchar_t[n.size() + 1];
    std::use_facet< std::ctype<wchar_t> >(loc).widen(n.data(), n.data() + n.size() + 1, w);
    ws = std::wstring(w);
    return ws;
}

std::string narrowString(std::wstring w) {
    std::string s;
    std::locale loc;
    char* n = new char[w.size() + 1];
    std::use_facet< std::ctype<wchar_t> >(loc).narrow(w.data(), w.data() + w.size() + 1, '?', n);
    s = std::string(n);
    return s;
}


bool reorderNameSpaceProviders(GUID *first) {
    WSADATA wsaData;
    int iResult;
    int iError = 0;

    INT iNuminfo = 0;

    // Allocate a 4K buffer to retrieve all the namespace providers
    DWORD dwInitialBufferLen = 4096;
    DWORD dwBufferLen;
    WSANAMESPACE_INFO* lpProviderInfo;

    // variables needed for converting provider GUID to a string
    int iRet = 0;
    WCHAR GuidString[40] = {0};

    // Set dwBufferLen to the initial buffer length
    dwBufferLen = dwInitialBufferLen;

    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) {
        wprintf(L"WSAStartup failed: %d\n", iResult);
        return 1;
    }

    lpProviderInfo = new WSANAMESPACE_INFO[dwBufferLen];

    iNuminfo = WSAEnumNameSpaceProviders(&dwBufferLen, lpProviderInfo);

    std::vector<GUID> providerGUIDs;

    for(int i = 0; i < iNuminfo; i++) {
        if(guidToString(first) != guidToString(&lpProviderInfo[i].NSProviderId)) {
            GUID g;
            memcpy(&g, &lpProviderInfo[i].NSProviderId, sizeof(GUID));
            providerGUIDs.push_back(g);
        }
    };
    GUID g;
    memcpy(&g, first, sizeof(GUID));
    providerGUIDs.insert(providerGUIDs.begin(), g);
    std::cout << "reordering namespaces: result=" << WSCWriteNameSpaceOrder(&providerGUIDs.front(), iNuminfo) << std::endl;
    return true;
}

std::wstring describeNameSpace(unsigned ns) {
    static std::map<unsigned, std::wstring> nsmap;
    if(!(ns & NS_ALL | NS_DNS | NS_WINS | NS_NETBT | NS_NTDS | NS_NLA | NS_BTH | NS_EMAIL | NS_PNRPNAME | NS_PNRPCLOUD)) {
        std::wstringstream wss;
        wss << L"Unknown namespace: " << ns;
        return wss.str();
    }
    nsmap[NS_ALL] = L"All possible namespaces (NS_ALL)";
    nsmap[NS_DNS] = L"Domain Name System (NS_DNS)";
    nsmap[NS_WINS] = L"Windows Internet Naming Service (NS_WINS)";
    nsmap[NS_NETBT] = L"NetBIOS (NS_NETBT)";
    nsmap[NS_NTDS] = L"Windows NT Directory Services (NS_NTDS)";
    nsmap[NS_NLA] = L"Network Location Awareness (NS_NLA)";
    nsmap[NS_BTH] = L"Bluetooth (NS_BTH)";
    nsmap[NS_EMAIL] = L"Email (NS_EMAIL)";
    nsmap[NS_PNRPNAME] = L"Peer-to-peer (NS_PNRPNAME)";
    nsmap[NS_PNRPCLOUD] = L"Peer-to-peer collection (NS_PNRPCLOUD)";
    return nsmap[ns];
}

std::wstring describeSocketFamily(unsigned sf) {
    static std::map<unsigned, std::wstring> sfmap;
    if(!(sf & AF_INET | AF_INET6)) {
        std::wstringstream wss;
        wss << L"unknown socket family: " << sf;
        return wss.str();
    }
    sfmap[AF_INET] = L"AF_INET";
    sfmap[AF_INET6] = L"AF_INET6";
    return sfmap[sf];
}

std::wstring describeSocketType(unsigned t) {
    if(t == SOCK_STREAM) return std::wstring(L"SOCK_STREAM");
    if(t == SOCK_DGRAM) return std::wstring(L"SOCK_DGRAM");
    if(t == SOCK_RDM) return std::wstring(L"SOCK_RDM");
    if(t == SOCK_SEQPACKET) return std::wstring(L"SOCK_SEQPACKET");
    if(t == SOCK_RAW) return std::wstring(L"SOCK_RAW");
    std::wstringstream wss;
    wss << L"unknown socket type: " << t;
    return wss.str();
}

std::wstring describeProtocol(unsigned p) {
    std::wstringstream wss;
    if(p == IPPROTO_TCP) return std::wstring(L"IPPROTO_TCP");
    if(p == IPPROTO_UDP) return std::wstring(L"IPPROTO_UDP");
    wss << L"unknown protocol: " << p;
    return wss.str();
}


#define CHECK_WSTRING(x) ((x != 0) ? x : L"<empty>")
#define CHECK_STRING(x) ((x != 0) ? x : "<empty>")

std::wstring dumpQuerySet(WSAQUERYSETW* wqs, bool singleline) {
    std::wstringstream wss;
    std::wstring ret;

    wchar_t eolChar = (singleline) ? L' ' : L'\n';
    wchar_t tabChar = (singleline) ? L' ' : L'\t';

    wss << L"size: " << wqs->dwSize << eolChar
        << L"serviceInstanceName: " << CHECK_WSTRING(wqs->lpszServiceInstanceName) << eolChar
        << L"serviceClassId: " << guidToWString(wqs->lpServiceClassId) << eolChar
        << L"version: " << wqs->lpVersion << eolChar
        << L"comment: " << CHECK_WSTRING(wqs->lpszComment) << eolChar
        << L"namespace: " << describeNameSpace(wqs->dwNameSpace) << eolChar
        << L"nsproviderid: " << guidToWString(wqs->lpNSProviderId) << eolChar
        << L"context: " << CHECK_WSTRING(wqs->lpszContext) << eolChar
        << L"numberOfProtocols: " << wqs->dwNumberOfProtocols << eolChar;
    unsigned n = wqs->dwNumberOfProtocols;
    if(n > 0 && wqs->lpafpProtocols != 0) {
        for(unsigned i = 0; i < n; ++i) {
            int family = wqs->lpafpProtocols[i].iAddressFamily;
            int protocol = wqs->lpafpProtocols[i].iProtocol;
            wss << tabChar << L"(" << i << L")" << eolChar;
            wss << tabChar << tabChar << L"family: " << describeSocketFamily(family) << eolChar;
            wss << tabChar << tabChar << L"protocol: " << describeProtocol(protocol) << eolChar;
        };
    }
    wss << L"query string: " << CHECK_WSTRING(wqs->lpszQueryString) << eolChar
        << L"numberOfCAddrs: " << wqs->dwNumberOfCsAddrs << eolChar;
    for(unsigned i = 0; i < wqs->dwNumberOfCsAddrs; i++) {
        wss << tabChar << L"(" << (i + 1) << L")" << eolChar;
        int socket_type = wqs->lpcsaBuffer[i].iSocketType;
        int socket_protocol = wqs->lpcsaBuffer[i].iProtocol;
        SOCKET_ADDRESS lsa = wqs->lpcsaBuffer[i].LocalAddr;
        SOCKET_ADDRESS rsa = wqs->lpcsaBuffer[i].RemoteAddr;
        WCHAR buffer[200];
        DWORD size = 200;
        if(lsa.lpSockaddr) {
            ZeroMemory(buffer, sizeof(buffer));
            WSAAddressToStringW(lsa.lpSockaddr, lsa.iSockaddrLength, NULL, buffer, &size);
            wss << tabChar << tabChar << L"local:       " << describeSocketFamily(lsa.lpSockaddr->sa_family) << L" " << buffer << eolChar;
        }
        size = 200;
        if(rsa.lpSockaddr) {
            ZeroMemory(buffer, sizeof(buffer));
            if(WSAAddressToStringW(rsa.lpSockaddr, rsa.iSockaddrLength, NULL, buffer, &size) == SOCKET_ERROR) {
                int res = WSAGetLastError();
                CHECK_RESULT(res, WSAEFAULT)
                CHECK_RESULT(res, WSAEINVAL)
                CHECK_RESULT(res, WSANOTINITIALISED)
                CHECK_RESULT(res, WSAENOBUFS)
                std::cout << "res: " << res << std::endl;
            }
            wss << tabChar << tabChar << L"remote:      " << describeSocketFamily(rsa.lpSockaddr->sa_family) << " " << buffer << eolChar;
            if(rsa.lpSockaddr->sa_family == AF_INET6) {
                struct sockaddr_in6* s_in6 = (struct sockaddr_in6*)rsa.lpSockaddr;
                wss << L"ipv6: " << std::hex;
                int b = 0;
                wss << s_in6->sin6_addr.u.Byte[b++] << s_in6->sin6_addr.u.Byte[b++] << L"::";
                b += 3 * 2;
                wss << s_in6->sin6_addr.u.Byte[b++] << s_in6->sin6_addr.u.Byte[b++] << L":";
                wss << s_in6->sin6_addr.u.Byte[b++] << s_in6->sin6_addr.u.Byte[b++] << L":";
                wss << s_in6->sin6_addr.u.Byte[b++] << s_in6->sin6_addr.u.Byte[b++] << L":";
                wss << s_in6->sin6_addr.u.Byte[b++] << s_in6->sin6_addr.u.Byte[b++] << L"%";
                wss << std::dec << s_in6->sin6_scope_id << L" port " << s_in6->sin6_port << L" flow info: " << s_in6->sin6_flowinfo << eolChar;
            }
        }

        wss << tabChar << tabChar << L"socket type: " << describeSocketType(socket_type) << eolChar;
        wss << tabChar << tabChar << L"protocol:    " << describeProtocol(socket_protocol) << eolChar;
    };

    wss << L"output flags: " << wqs->dwOutputFlags << eolChar
        << L"blob: " << wqs->lpBlob;
    if(wqs->lpBlob) {
        wss << " size: " << wqs->lpBlob->cbSize;
    }
    wss << eolChar;
    ret = wss.str();
    return ret;
}

int copyQuerySetToBuffer(WSAQUERYSETW* dst, WSAQUERYSETW* src) {
    BYTE* ptr = (BYTE*)dst;
    *dst = *src;
    ptr += sizeof(WSAQUERYSETW);

    if(src->lpszServiceInstanceName) {
        dst->lpszServiceInstanceName = (LPWSTR)ptr;
        const int l = wcslen(src->lpszServiceInstanceName) + 1;
        wcsncpy(dst->lpszServiceInstanceName, src->lpszServiceInstanceName, l);
        ptr += l * sizeof(WCHAR);
    }

    if(src->lpServiceClassId) {
        dst->lpServiceClassId  = (LPGUID)ptr;
        *dst->lpServiceClassId = *src->lpServiceClassId;
        ptr += sizeof(GUID);
    }

    if(src->lpVersion) {
        dst->lpVersion  = (LPWSAVERSION)ptr;
        *dst->lpVersion = *src->lpVersion;
        ptr += sizeof(WSAVERSION);
    }

    if(src->lpszComment) {
        dst->lpszComment = (LPWSTR)ptr;
        const int l = wcslen(src->lpszComment) + 1;
        wcsncpy(dst->lpszComment, src->lpszComment, l);
        ptr += l * sizeof(WCHAR);
    }

    if(src->lpNSProviderId) {
        dst->lpNSProviderId  = (LPGUID)ptr;
        *dst->lpNSProviderId = *src->lpNSProviderId;
        ptr += sizeof(GUID);
    }

    if(src->lpszContext) {
        dst->lpszContext = (LPWSTR)ptr;
        const int l = wcslen(src->lpszContext) + 1;
        wcsncpy(dst->lpszContext, src->lpszContext, l);
        ptr += l * sizeof(WCHAR);
    }

    const unsigned nop = src->dwNumberOfProtocols;
    if(nop > 0 && src->lpafpProtocols) {
        dst->lpafpProtocols = (LPAFPROTOCOLS)ptr;
        for(unsigned i = 0; i < nop; ++i) dst->lpafpProtocols[i] = src->lpafpProtocols[i];
        ptr += nop * sizeof(AFPROTOCOLS);
    }

    if(src->lpszQueryString) {
        dst->lpszQueryString = (LPWSTR)ptr;
        const int l = wcslen(src->lpszQueryString) + 1;
        wcsncpy(dst->lpszQueryString, src->lpszQueryString, l);
        ptr += l * sizeof(WCHAR);
    }

    const unsigned noa = src->dwNumberOfCsAddrs;
    if(noa > 0) {
        dst->lpcsaBuffer = (LPCSADDR_INFO)ptr;
        ptr += sizeof(CSADDR_INFO) * noa;
    }

    for(unsigned index = 0; index < noa; index++) {
        dst->lpcsaBuffer[index] = src->lpcsaBuffer[index];

        dst->lpcsaBuffer[index].LocalAddr.lpSockaddr = (LPSOCKADDR)ptr;
        memcpy(ptr, src->lpcsaBuffer[index].LocalAddr.lpSockaddr, src->lpcsaBuffer[index].LocalAddr.iSockaddrLength);
        ptr += src->lpcsaBuffer[index].LocalAddr.iSockaddrLength;
        dst->lpcsaBuffer[index].RemoteAddr.lpSockaddr = (LPSOCKADDR)ptr;
        memcpy(ptr, src->lpcsaBuffer[index].RemoteAddr.lpSockaddr, src->lpcsaBuffer[index].RemoteAddr.iSockaddrLength);
        ptr += src->lpcsaBuffer[index].RemoteAddr.iSockaddrLength;
    }

    if(src->lpBlob) {
        dst->lpBlob = (LPBLOB)ptr;
        *dst->lpBlob = *src->lpBlob;
        ptr += sizeof(BLOB);
        memcpy(dst->lpBlob->pBlobData, src->lpBlob->pBlobData, src->lpBlob->cbSize);
        ptr += src->lpBlob->cbSize;
    }
    return (ptr - (BYTE*)dst);
}

int calculateQuerySetSize(WSAQUERYSETW* wqs, DWORD inFlags, bool required) {
    int size = sizeof(WSAQUERYSETW);
    if((LUP_RETURN_NAME & inFlags) && wqs->lpszServiceInstanceName) {
        size += sizeof(WCHAR) * (wcslen(wqs->lpszServiceInstanceName) + 1);
    }

    if(wqs->lpServiceClassId) {
        size += sizeof(*wqs->lpServiceClassId);
    }

    if(wqs->lpVersion) {
        size += sizeof(*wqs->lpVersion);
    }

    if(wqs->lpszComment) {
        size += sizeof(WCHAR) * (wcslen(wqs->lpszComment) + 1);
    }

    if(wqs->lpNSProviderId) {
        size += sizeof(*wqs->lpNSProviderId);
    }

    if(wqs->lpszContext) {
        size += sizeof(WCHAR) * (wcslen(wqs->lpszContext) + 1);
    }

    size += wqs->dwNumberOfProtocols * sizeof(AFPROTOCOLS);

    if(wqs->lpszQueryString) {
        size += sizeof(WCHAR) * (wcslen(wqs->lpszQueryString) + 1);
    }

    if(required) {
        if(inFlags & LUP_RETURN_ADDR) {
            size += 2 * sizeof(CSADDR_INFO);
            size += 2 * sizeof(struct sockaddr_in);
        }
    } else {
        size += sizeof(CSADDR_INFO) * wqs->dwNumberOfCsAddrs;
        for(unsigned index = 0; index < wqs->dwNumberOfCsAddrs; index++) {
            if(wqs->lpcsaBuffer[index].LocalAddr.lpSockaddr) size += wqs->lpcsaBuffer[index].LocalAddr.iSockaddrLength;
            if(wqs->lpcsaBuffer[index].RemoteAddr.lpSockaddr) size += wqs->lpcsaBuffer[index].RemoteAddr.iSockaddrLength;
        }
    }

    if(required) {
        if(inFlags & LUP_RETURN_BLOB) {
            size += sizeof(BLOB);
            size += sizeof(struct hostent);
            size += wcslen(wqs->lpszServiceInstanceName) + 1;
            size += 4;  // Alias list terminator (0 offset)
            size += 4;  // Offset to address.
            size += 4;  // Address list terminator (0 offset)
            size += 4;  // IPv4 address
        }
    } else {
        if(wqs->lpBlob) {
            size += sizeof(BLOB);
            size += wqs->lpBlob->cbSize;
        }
    }
    return size;
}
