#include <algorithm>
#include <string>
#include <sstream>

#include <windows.h>
#include <Ws2spi.h>
#include <Ws2ipdef.h>

#include "helpers.h"

HINSTANCE g_dll;

extern "C" {
    void CALLBACK InstallNSPW(HWND hwnd, HINSTANCE hinst, LPWSTR lpszCmdLine,
                              int nCmdShow);
    void CALLBACK UnInstallNSPW(HWND hwnd, HINSTANCE hinst, LPWSTR lpszCmdLine,
                              int nCmdShow);
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved) {
    g_dll = hinstDLL;
    return TRUE;
}

void CALLBACK InstallNSPW(HWND hwnd, HINSTANCE hinst, LPWSTR lpszCmdLine,
                          int nCmdShow) {
    // {F80B6F58-544F-4797-B27B-E46D4F294540}
    GUID guid = { 0xf80b6f58, 0x544f, 0x4797, { 0xb2, 0x7b, 0xe4, 0x6d, 0x4f, 0x29, 0x45, 0x40 } };
    WCHAR* str = L"wsp-mdns: namespace provider based on avahi";
    WCHAR path[1024];
    GetModuleFileNameW(g_dll, path, 1024);
    DWORD ns = NS_DNS, ns_version = 0;

    if(SOCKET_ERROR == WSCInstallNameSpace(str, path, ns, ns_version, &guid)) {
        OutputDebugStringW(L"installation of namespace failed!");
    } else {
        OutputDebugStringW(L"successfully installed nsp!");
    }

    // put our namespace provider to the top so that it is called before the standard dns provider
    reorderNameSpaceProviders(&guid);
}

void CALLBACK UnInstallNSPW(HWND hwnd, HINSTANCE hinst, LPWSTR lpszCmdLine,
                          int nCmdShow) {
    // {F80B6F58-544F-4797-B27B-E46D4F294540}
    GUID guid = { 0xf80b6f58, 0x544f, 0x4797, { 0xb2, 0x7b, 0xe4, 0x6d, 0x4f, 0x29, 0x45, 0x40 } };
    if(SOCKET_ERROR == WSCUnInstallNameSpace(&guid)) {
        OutputDebugStringW(L"uninstallation of namespace failed!");
    } else {
        OutputDebugStringW(L"successfully uninstalled nsp!");
    }
}

int WSPAPI NSPCleanup(GUID* inProviderID) {
    return NO_ERROR;
}

typedef struct _query {
    _query() : finished(false)
    {}
    WSAQUERYSETW* wqs;
    int wqs_size;
    bool finished;
    DWORD lastFlags;
} dnsquery;

int WSPAPI NSPLookupServiceBegin(GUID* inProviderID, WSAQUERYSETW* inQuerySet,
                                WSASERVICECLASSINFOW* inServiceClassInfo,
                                DWORD inFlags, HANDLE* outLookup) {
    std::wstringstream wss;

    if(inQuerySet == 0) {
        WSASetLastError(WSAEINVAL);
        return SOCKET_ERROR;
    }
    if(outLookup == 0) {
        WSASetLastError(WSAEINVAL);
        return SOCKET_ERROR;
    }

    if((inFlags & (LUP_RETURN_ADDR | LUP_RETURN_BLOB)) == 0) {
        WSASetLastError(WSASERVICE_NOT_FOUND);
        return SOCKET_ERROR;
    }
    DWORD type = inQuerySet->dwNameSpace;
    if((type != NS_DNS) && (type != NS_ALL)) {
        WSASetLastError(WSASERVICE_NOT_FOUND);
        return SOCKET_ERROR;
    }
    int n = inQuerySet->dwNumberOfProtocols;
    if(n > 0) {
        if(inQuerySet->lpafpProtocols == 0) {
            WSASetLastError(WSAEINVAL);
            return SOCKET_ERROR;
        }

        int i = 0;
        for(; i < n; ++i) {
            int family = inQuerySet->lpafpProtocols[i].iAddressFamily;
            int protocol = inQuerySet->lpafpProtocols[i].iProtocol;
            if(((family == AF_INET) || (family == AF_INET6))
            && ((protocol == IPPROTO_UDP) || (protocol == IPPROTO_TCP))) {
                break;
            }
        };
        if(i == n) {
            WSASetLastError(WSASERVICE_NOT_FOUND);
            return SOCKET_ERROR;
        }
    }

    if(inQuerySet->lpszServiceInstanceName == 0) {
        WSASetLastError(WSAEINVAL);
        return SOCKET_ERROR;
    }

    std::wstring name = std::wstring(inQuerySet->lpszServiceInstanceName);

    std::transform(name.begin(), name.end(), name.begin(), ::tolower);
    // FQDN can have a '.' at the end
    if(*name.rbegin() == L'.') {
        name = name.substr(0, name.length() - 1);
    }

    if(name.length() <= 8 || 0 != name.compare(name.length() - 8, 8, L".patrick")) {
        WSASetLastError(WSASERVICE_NOT_FOUND);
        return SOCKET_ERROR;
    }

//     wss << dumpQuerySet(inQuerySet, true);
//     OutputDebugStringW(wss.str().c_str());
//     wss.str(L"");
//
//     OutputDebugStringW((name + L" has been accepted!").c_str());

    dnsquery* dq = new dnsquery;
    const int wqs_size = calculateQuerySetSize(inQuerySet, inFlags, false);
    dq->wqs_size = wqs_size;
    dq->wqs = (WSAQUERYSETW*)new BYTE[wqs_size];
    const int copied_size = copyQuerySetToBuffer(dq->wqs, inQuerySet);
    dq->lastFlags = inFlags;
    *outLookup = (HANDLE)dq;
    return NO_ERROR;
}

int WSPAPI NSPLookupServiceNext(HANDLE inLookup, DWORD inFlags,
                                DWORD* ioBufferLength,
                                WSAQUERYSETW* outResults) {
    dnsquery* dq = (dnsquery*)inLookup;
    DWORD ipv4 = htonl(0xC0A80269);
    

    if(dq->finished) {
//         OutputDebugStringW(L"no more data to be found!");
        WSASetLastError(WSA_E_CANCELLED);
        return SOCKET_ERROR;
    }

    DWORD useFlags = inFlags;
    if(inFlags == 0) {
        useFlags = dq->lastFlags;
    }

//     std::wstringstream wss;
    unsigned requiredSize = calculateQuerySetSize(dq->wqs, useFlags);
    if(requiredSize > *ioBufferLength) {
//         OutputDebugStringW(L"not enough buffer size!");
        WSASetLastError(WSAEFAULT);
        return SOCKET_ERROR;
    }

    *outResults = *dq->wqs;
    BYTE* ptr = (BYTE*)outResults + sizeof(WSAQUERYSETW);

    if(useFlags & LUP_RETURN_NAME) {
        if(dq->wqs->lpszServiceInstanceName) {
            outResults->lpszServiceInstanceName = (LPWSTR)ptr;
            const int l = wcslen(dq->wqs->lpszServiceInstanceName) + 1;
            wcsncpy(outResults->lpszServiceInstanceName, dq->wqs->lpszServiceInstanceName, l);
            ptr += l * sizeof(WCHAR);
        }
    } else {
        outResults->lpszServiceInstanceName = NULL;
    }

    if(dq->wqs->lpServiceClassId) {
        outResults->lpServiceClassId  = (LPGUID)ptr;
        *outResults->lpServiceClassId = *dq->wqs->lpServiceClassId;
        ptr += sizeof(GUID);
    }

    if(dq->wqs->lpVersion) {
        outResults->lpVersion  = (LPWSAVERSION)ptr;
        *outResults->lpVersion = *dq->wqs->lpVersion;
        ptr += sizeof(WSAVERSION);
    }

    if(dq->wqs->lpszComment) {
        outResults->lpszComment = (LPWSTR)ptr;
        const int l = wcslen(dq->wqs->lpszComment) + 1;
        wcsncpy(outResults->lpszComment, dq->wqs->lpszComment, l);
        ptr += l * sizeof(WCHAR);
    }

    if(dq->wqs->lpNSProviderId) {
        outResults->lpNSProviderId  = (LPGUID)ptr;
        *outResults->lpNSProviderId = *dq->wqs->lpNSProviderId;
        ptr += sizeof(GUID);
    }

    if(dq->wqs->lpszContext) {
        outResults->lpszContext = (LPWSTR)ptr;
        const int l = wcslen(dq->wqs->lpszContext) + 1;
        wcsncpy(outResults->lpszContext, dq->wqs->lpszContext, l);
        ptr += l * sizeof(WCHAR);
    }

    const int nop = dq->wqs->dwNumberOfProtocols;
    if(nop > 0 && dq->wqs->lpafpProtocols) {
        outResults->lpafpProtocols = (LPAFPROTOCOLS)ptr;
        for(int i = 0; i < nop; ++i) outResults->lpafpProtocols[i] = dq->wqs->lpafpProtocols[i];
        ptr += nop * sizeof(AFPROTOCOLS);
    } else {
        dq->wqs->lpafpProtocols = NULL;
    }

    if(dq->wqs->lpszQueryString) {
        outResults->lpszQueryString = (LPWSTR)ptr;
        const int l = wcslen(dq->wqs->lpszQueryString) + 1;
        wcsncpy(outResults->lpszQueryString, dq->wqs->lpszQueryString, l);
        ptr += l * sizeof(WCHAR);
    }

    if(useFlags & LUP_RETURN_ADDR) {
        int index = 0;

        outResults->dwNumberOfCsAddrs = 2;
        outResults->lpcsaBuffer = (LPCSADDR_INFO)ptr;
        ptr += sizeof(CSADDR_INFO) * 2;

        // once for ipv4 and once for ipv6
        outResults->lpcsaBuffer[index].LocalAddr.lpSockaddr = NULL;
        outResults->lpcsaBuffer[index].LocalAddr.iSockaddrLength = 0;
        outResults->lpcsaBuffer[index].RemoteAddr.lpSockaddr = (LPSOCKADDR)ptr;
        outResults->lpcsaBuffer[index].RemoteAddr.iSockaddrLength = sizeof(struct sockaddr_in);
        outResults->lpcsaBuffer[index].iSocketType = SOCK_DGRAM;
        outResults->lpcsaBuffer[index].iProtocol = IPPROTO_UDP;

        struct sockaddr_in* s_in = (struct sockaddr_in*)ptr;
        ZeroMemory(s_in, sizeof(struct sockaddr_in));
        s_in->sin_family = AF_INET;
        s_in->sin_addr.S_un.S_addr = ipv4;
        ptr += sizeof(struct sockaddr_in);
        index++;
        outResults->lpcsaBuffer[index].LocalAddr.lpSockaddr = NULL;
        outResults->lpcsaBuffer[index].LocalAddr.iSockaddrLength = 0;
        outResults->lpcsaBuffer[index].RemoteAddr.lpSockaddr = (LPSOCKADDR)ptr;
        outResults->lpcsaBuffer[index].RemoteAddr.iSockaddrLength = sizeof(struct sockaddr_in6);
        outResults->lpcsaBuffer[index].iSocketType = SOCK_DGRAM;
        outResults->lpcsaBuffer[index].iProtocol = IPPROTO_UDP;

        struct sockaddr_in6* s_in6 = (struct sockaddr_in6*)ptr;
        ZeroMemory(s_in6, sizeof(struct sockaddr_in6));
        s_in6->sin6_family = AF_INET6;
        ZeroMemory(s_in6->sin6_addr.u.Byte, 16);
        int b = 0;
        // FIXME:remove hardcoded address
        s_in6->sin6_addr.u.Byte[b++]=0xfe;
        s_in6->sin6_addr.u.Byte[b++]=0x80;
        b += 3 * 2;
        s_in6->sin6_addr.u.Byte[b++]=0x02;
        s_in6->sin6_addr.u.Byte[b++]=0x1e;
        s_in6->sin6_addr.u.Byte[b++]=0x65;
        s_in6->sin6_addr.u.Byte[b++]=0xff;
        s_in6->sin6_addr.u.Byte[b++]=0xfe;
        s_in6->sin6_addr.u.Byte[b++]=0x29;
        s_in6->sin6_addr.u.Byte[b++]=0xe1;
        s_in6->sin6_addr.u.Byte[b++]=0xae;
        s_in6->sin6_scope_id = 13;
        ptr += sizeof(struct sockaddr_in6);
    }

    outResults->dwOutputFlags = RESULT_IS_ADDED;

    if(useFlags & LUP_RETURN_BLOB) {
        uintptr_t *p;

        outResults->lpBlob = (LPBLOB)ptr;
        ptr += sizeof(BLOB);
        BYTE *base = ptr;
        outResults->lpBlob->pBlobData = (BYTE*)base;

        struct hostent *he = (struct hostent*)ptr;
        ptr += sizeof(struct hostent);

        he->h_name = (char*)(ptr - base);  // relative to the base!
        const int nl = wcslen(dq->wqs->lpszServiceInstanceName) + 1;
        std::string ts = narrowString(dq->wqs->lpszServiceInstanceName);
        memcpy(ptr, ts.c_str(), nl);
        ptr += nl;

        he->h_aliases = (char**)(ptr - base);
        p = (uintptr_t*)ptr;
        *p = 0;
        ptr += sizeof(char*);


        he->h_addrtype = AF_INET;
        he->h_length = 4;

        he->h_addr_list = (char **)(ptr - base);
        p = (uintptr_t*)ptr;
        ptr += 2 * sizeof(uintptr_t);
        *p++ = (uintptr_t)(ptr - base);
        *p++ = 0;

        *(uintptr_t*)ptr = (uintptr_t)ipv4;
        ptr += sizeof(uintptr_t);
        outResults->lpBlob->cbSize = (ULONG)(ptr - base);
    }

    dq->finished = true;

//     std::wstring ws = dumpQuerySet(outResults, true);
//     OutputDebugStringW(ws.c_str());

    return NO_ERROR;
}

int WSPAPI NSPLookupServiceEnd(HANDLE inLookup) {
    dnsquery* dq = (dnsquery*)inLookup;
    delete[] (BYTE*)dq->wqs;
    delete dq;
    return NO_ERROR;
}

int WSPAPI NSPSetService(GUID* inProviderID,
                         WSASERVICECLASSINFOW* inServiceClassInfo, 
                         WSAQUERYSETW* inRegInfo, WSAESETSERVICEOP inOperation,
                         DWORD inFlags) {
    WSASetLastError(WSAEOPNOTSUPP);
    return SOCKET_ERROR;
}

int WSPAPI NSPInstallServiceClass(GUID* inProviderID,
                                  WSASERVICECLASSINFOW* inServiceClassInfo) {
    WSASetLastError(WSAEOPNOTSUPP);
    return SOCKET_ERROR;
}

int WSPAPI NSPRemoveServiceClass(GUID* inProviderID, GUID* inServiceClassID) {
    WSASetLastError(WSAEOPNOTSUPP);
    return SOCKET_ERROR;
}
int WSPAPI NSPGetServiceClassInfo(GUID* inProviderID, DWORD* ioBufSize,
                                  WSASERVICECLASSINFOW* ioServiceClassInfo ) {
    WSASetLastError(WSAEOPNOTSUPP);
    return SOCKET_ERROR;
}

int WSPAPI NSPIoctl(HANDLE hLookup, DWORD dwControlCode, LPVOID lpvInBuffer,
                    DWORD cbInBuffer, LPVOID lpvOutBuffer, DWORD cbOutBuffer,
                    LPDWORD lpcbBytesReturned, LPWSACOMPLETION lpCompletion,
                    LPWSATHREADID   lpThreadId) {
    WSASetLastError(WSAEOPNOTSUPP);
    return SOCKET_ERROR;
}


int WSPAPI NSPStartup(GUID* inProviderID, NSP_ROUTINE* outRoutines) {
    outRoutines->cbSize = sizeof(NSP_ROUTINE);
    outRoutines->dwMajorVersion = 0;
    outRoutines->dwMinorVersion = 0;
    outRoutines->NSPCleanup = NSPCleanup;
    outRoutines->NSPLookupServiceBegin = NSPLookupServiceBegin;
    outRoutines->NSPLookupServiceNext = NSPLookupServiceNext;
    outRoutines->NSPLookupServiceEnd = NSPLookupServiceEnd;
    outRoutines->NSPSetService = NSPSetService;
    outRoutines->NSPInstallServiceClass = NSPInstallServiceClass;
    outRoutines->NSPRemoveServiceClass = NSPRemoveServiceClass;
    outRoutines->NSPGetServiceClassInfo = NSPGetServiceClassInfo;
    outRoutines->NSPIoctl = NSPIoctl;
    return NO_ERROR;
}
