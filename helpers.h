#ifndef helpers_h
#define helpers_h

#include <string>
#include <windows.h>
#include <winsock2.h>

bool stringToGUID(const std::string& guid, GUID* output);
std::wstring guidToWString(GUID* guid);
std::string guidToString(GUID* guid);

std::wstring widenString(std::string);
std::string narrowString(std::wstring);

bool reorderNameSpaceProviders(GUID *first);

int calculateQuerySetSize(WSAQUERYSETW* wqs, DWORD inFlags, bool required=true);
int copyQuerySetToBuffer(WSAQUERYSETW* dst, WSAQUERYSETW* src);

std::wstring dumpQuerySet(WSAQUERYSETW* wqs, bool singleline=false);

#endif /* helpers_h */
